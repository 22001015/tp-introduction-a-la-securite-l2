#include <stdio.h>
#include "bignum.h"

unsigned long int gcd(unsigned long int a, unsigned long int b){
  unsigned long int old_r, r = 1;
  if(a < b)
    return gcd(b,a);

  while(r != 0){
    old_r = r;
    r = a % b;
    a = b;
    b = r;
  }

  return old_r;
}

unsigned long int f(unsigned long int x){
  return (x*x) + 1 ;
}

unsigned long int facto(unsigned long int a){
  unsigned long int i = 1l;
  while (gcd(a, i) == 1){
    i ++ ;
    //i =  f(i);
    //printf("%lu\n", i);
  }
  return gcd(a, i);
}

int main() {
  // En utilisant l'algorithme rho de Pollard, factorisez les entiers suivants

  unsigned long int n1 = 17 * 113;
  unsigned long int n2 = 239 * 431;
  unsigned long int n3 = 3469 * 4363;
  unsigned long int n4 = 15241 * 18119;
  unsigned long int n5 = 366127l * 416797l;
  unsigned long int n6 = 15651941l * 15485863l;

  bignum_t n7, n8;

  n7 = bignum_mul(bignum_sub(bignum_pow(bignum_from_int(2), bignum_from_int(127)),
                             bignum_from_int(1)),
                  bignum_sub(bignum_pow(bignum_from_int(2), bignum_from_int(61)),
                             bignum_from_int(1)));

  n8 = bignum_mul(bignum_sub(bignum_pow(bignum_from_int(2), bignum_from_int(607)),
                             bignum_from_int(1)),
                  bignum_sub(bignum_pow(bignum_from_int(2), bignum_from_int(2203)),
                             bignum_from_int(1)));


  printf("\n-------------------------------------\n");
  printf("n1 : %lu\n", facto(n1));
  printf("n2 : %lu\n", facto(n2));
  printf("n3 : %lu\n", facto(n3));
  printf("n4 : %lu\n", facto(n4));
  printf("n5 : %lu\n", facto(n5));
  printf("n6 : %lu\n", facto(n6));



  //printf("n7 : %lu\n", facto(n7));
  //printf("n8 : %lu\n", facto(n8));

  printf("PGCD(42,24) = %lu\n", gcd(42,24));
  printf("PGCD(42,24) = %s\n", bignum_to_str(bignum_gcd(bignum_from_int(42),bignum_from_int(24))));

  printf("\n-------------------------------------\n");

  return 0;
}
