
#include <stdio.h>
#include <stdlib.h>

struct cell_t {
  void* val;
  unsigned long int id;
  struct cell_t* next;
};

typedef struct cell_t* list_t;

list_t list_empty(){
  return NULL;
}

int list_is_empty(list_t l){
  return (l == NULL);
}

list_t list_tail(list_t l){
  if (!list_is_empty(l)){
    return l -> next ;
  }
  return NULL ;
}


/*
list_t list_push(list_t l, void* x){
  list_t head = malloc(sizeof(struct cell_t));
  head -> val = x ;
  head -> next = l ;
  if (list_is_empty(l)) {
    head -> id = 1 ;
  }
  else {
    head -> id = l -> id + 1 ;
  }
  //list_is_empty(l) ? 1 : l -> id + 1 ;
  return head ;
}*/

/*
list_t list_push(list_t l, void* x){
  list_t head = malloc(sizeof(struct cell_t));
  head = l ;
  while (!list_is_empty(l)) {
    l = list_tail(l) ;
  }
  l -> val = x ;
  l -> next = NULL ;

  return head ;
}*/

list_t list_push(list_t l, void* x){
  list_t res = malloc(sizeof(struct cell_t));
  res->next = l;
  res->val = x;
  res->id = list_is_empty(l) ? 1 : 1 + l->id;
  return res;
}



// return the found element or NULL
/*
void* list_in(list_t l, void* x, int (*eq)(void*, void*)){
  while (!list_is_empty(l)) {
    if (eq(x, l -> val)) {
      return l -> val;
    }
    l = list_tail(l) ;
  }
  return NULL ;
}*/

void* list_in(list_t l, void* x, int (*eq)(void*, void*)){
  while(!list_is_empty(l))
    if(eq(x, l->val))
      return l->val;
    else
      l = l->next;
  return NULL;
}


void* list_top(list_t l){
  //si liste vide, return NULL, sinon l -> val
  return list_is_empty(l) ? NULL : l -> val ;
}


/*
unsigned long int list_len(list_t l){
  while (list_tail(l) != NULL) {
    l = list_tail(l) ;
  }
  return l -> id ;
}*/

unsigned long int list_len(list_t l){
  return list_is_empty(l) ? 0 : l->id;
}


void* list_pop(list_t* l){
  if(list_is_empty(*l))
    return NULL;
  list_t new_head = (*l) -> next;
  void* res = (*l) -> val;
  free(*l);
  *l = new_head;
  return res;

}

void list_destroy(list_t l, void (*free)(void*)){
  while (!list_is_empty(l)) {
    free(list_pop(&l)) ;
  }
}
